import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/guidePage.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/guidePage',
    name: 'guidePage',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/guidePage.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login.vue')
  },
  {
    path: '/index',
    name: 'index',
    component: () => import('../views/index.vue')
  },
  {
    path: '/punishment',
    name: 'punishment',
    component: () => import('../views/index/punishment.vue')
  },
  {
    path: '/activity',
    name: 'activity',
    component: () => import('../views/index/activity.vue')
  }, {
    path: '/message',
    name: 'message',
    component: () => import('../views/message.vue')
  }, {
    path: '/school',
    name: 'school',
    component: () => import('../views/school.vue')
  }, {
    path: '/feedback',
    name: 'feedback',
    component: () => import('../views/index/feedback.vue')
  }, {
    path: '/newFeedback',
    name: 'newFeedback',
    component: () => import('../views/index/newFeedback.vue')
  }, {
    path: '/grade',
    name: 'grade',
    component: () => import('../views/index/grade.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
