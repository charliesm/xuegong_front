import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    usrInfo: {
      name: '张世明',
      id: '1180280051'
    }
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
